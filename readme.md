# Ansible Network Automation Platform Agnostic
> This project will host an example of a dynamic playbook that is platform agnostic

## Setup :
---
> Here are setup steps, but this is more of a reference/example repo
- Install ansible galaxy requirements
- `.vault_pass` is used to encrypt password to devices here.

## Files
---
- `configure-interface.yml` : playbook for configuring all interfaces on a host
  - `host_vars/<hostname>.yml` : Holds variables for configuration (and specific credentials if needed)
  - `tasks/<ansible_os>-interface.yml` : will be called dynamically per os for interface configuration

- `group_vars/<groupname>.yml` : Hosts are grouped by os, and these groups will tell ansible how to connect to each one


- `Makefile`:
  - `make interface` : wrapper for `ansible-playbook configure-interface.yml`
