#'makeke' by itself runs the 'run' target
.DEFAULT_GOAL := run

.PHONY: run
run: 
	@echo "Run placeholder"

.PHONY: interface
interface:
	ansible-playbook configure-interface.yml
